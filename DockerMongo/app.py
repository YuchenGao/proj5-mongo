import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import flask
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = 43432
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.route("/submit", methods=['POST'])
def submit():
    data = request.form
    km = [];
    open_time = [];
    close_time= [];
    dist = 0;
    db.tododb.remove({})                           
    for key in data.keys():

       for value in data.getlist(key):      
            if key == 'km' and value != '':     
                km.append(value)
            elif key == 'open' and value != '':
                open_time.append(value)
            elif key == 'close' and value != '':
                close_time.append(value)
            elif key == 'distance' and value != '':
                dist=value
              
    item_doc = {"km": km, "open":open_time,"close": close_time,"dist": dist}
    db.tododb.insert_one(item_doc)

    return render_template('calc.html')
    
    
@app.route('/done')
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]
    return render_template('todo.html', items=items)


###
# Pages
###




@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))


    date = request.args.get('date',"",type=str)
    time = request.args.get('time',"",type=str)
    dist = request.args.get('dist',200,type=int)
    date_str = date + " " + time
    start_time = arrow.get(date_str).isoformat
    
    error_msg = ""
    if (km != 0):
        check = (km-dist)/km

    check = 0
    if km == 0:
        error_msg = "Error! Control distance is at ZERO!"
    else:
        check = (km-dist)/km
    if check > 0.2 :
        error_msg = "Warning! Control distance is 20% or longer than brever"
    
    times = date + " " + time
    open_time = acp_times.open_time(km, 200,times)
    close_time = acp_times.close_time(km, 200,times)
    result = {"open": open_time, "close": close_time,"error_msg":error_msg}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
